#!/bin/sh
# A helper script to find out missing copyright data extracted from git repository
# data.
git log --format="%an;%ad" \
    | sed -e 's/\(.*\);.* \(....\) .*/\1;\2/' \
    | sort | uniq -c | sort -n -r

From 0990b7914b589758ffab805854c5ef996b4ba316 Mon Sep 17 00:00:00 2001
From: Nikhil Benesch <nikhil.benesch@gmail.com>
Date: Tue, 30 Jul 2019 14:52:39 -0400
Subject: [PATCH] Upgrade to url v2.0

--- a/Cargo.toml
+++ b/Cargo.toml
@@ -33,7 +33,7 @@
 version = "0.12.0"
 
 [dependencies.cookie_store]
-version = "0.7.0"
+version = "0.8"
 
 [dependencies.encoding_rs]
 version = "0.8"
@@ -72,12 +72,15 @@
 version = "0.3.7"
 
 [dependencies.mime_guess]
-version = "2.0.0-alpha.6"
+version = "2.0.0"
 
 [dependencies.native-tls]
 version = "0.2"
 optional = true
 
+[dependencies.percent-encoding]
+version = "2"
+
 [dependencies.rustls]
 version = "0.15"
 features = ["dangerous_configuration"]
@@ -90,7 +93,7 @@
 version = "1.0"
 
 [dependencies.serde_urlencoded]
-version = "0.5"
+version = "0.6"
 
 [dependencies.socks]
 version = "0.3.2"
@@ -125,10 +128,10 @@
 optional = true
 
 [dependencies.url]
-version = "1.2"
+version = "2"
 
 [dependencies.uuid]
-version = "0.7"
+version = "0.8"
 features = ["v4"]
 
 [dependencies.webpki-roots]
--- a/src/async_impl/multipart.rs
+++ b/src/async_impl/multipart.rs
@@ -3,7 +3,7 @@
 use std::fmt;
 
 use mime_guess::Mime;
-use url::percent_encoding::{self, EncodeSet, PATH_SEGMENT_ENCODE_SET};
+use percent_encoding::{self, AsciiSet};
 use uuid::Uuid;
 use http::HeaderMap;
 
@@ -371,31 +371,39 @@
     }
 }
 
-#[derive(Debug, Clone)]
-pub(crate) struct AttrCharEncodeSet;
+/// https://url.spec.whatwg.org/#fragment-percent-encode-set
+const FRAGMENT_ENCODE_SET: &AsciiSet = &percent_encoding::CONTROLS.add(b' ').add(b'"').add(b'<').add(b'>').add(b'`');
 
-impl EncodeSet for AttrCharEncodeSet {
-    fn contains(&self, ch: u8) -> bool {
-        match ch as char {
-             '!'  => false,
-             '#'  => false,
-             '$'  => false,
-             '&'  => false,
-             '+'  => false,
-             '-'  => false,
-             '.' => false,
-             '^'  => false,
-             '_'  => false,
-             '`'  => false,
-             '|'  => false,
-             '~' => false,
-              _ => {
-                  let is_alpha_numeric = ch >= 0x41 && ch <= 0x5a || ch >= 0x61 && ch <= 0x7a || ch >= 0x30 && ch <= 0x39;
-                  !is_alpha_numeric
-              }
-        }
-    }
-}
+/// https://url.spec.whatwg.org/#path-percent-encode-set
+const PATH_ENCODE_SET: &AsciiSet = &FRAGMENT_ENCODE_SET.add(b'#').add(b'?').add(b'{').add(b'}');
+
+const PATH_SEGMENT_ENCODE_SET: &AsciiSet = &PATH_ENCODE_SET.add(b'/').add(b'%');
+
+// This will be a bit shorter to express when AsciiSet.remove lands:
+// https://github.com/servo/rust-url/pull/528.
+/// https://tools.ietf.org/html/rfc8187#section-3.2.1
+const ATTR_CHAR_ENCODE_SET: &AsciiSet = &percent_encoding::CONTROLS
+    .add(b' ')
+    .add(b'"')
+    .add(b'%')
+    .add(b'\'')
+    .add(b'(')
+    .add(b')')
+    .add(b'*')
+    .add(b',')
+    .add(b'/')
+    .add(b':')
+    .add(b';')
+    .add(b'<')
+    .add(b'=')
+    .add(b'>')
+    .add(b'?')
+    .add(b'@')
+    .add(b'[')
+    .add(b'\\')
+    .add(b']')
+    .add(b'{')
+    .add(b'}');
 
 pub(crate) enum PercentEncoding {
     PathSegment,
@@ -443,7 +451,7 @@
                     .to_string()
             },
             PercentEncoding::AttrChar => {
-                percent_encoding::utf8_percent_encode(value, AttrCharEncodeSet)
+                percent_encoding::utf8_percent_encode(value, ATTR_CHAR_ENCODE_SET)
                     .to_string()
             },
             PercentEncoding::NoOp => { value.to_string() },
--- a/src/lib.rs
+++ b/src/lib.rs
@@ -193,6 +193,7 @@
 extern crate mime_guess;
 #[cfg(feature = "default-tls")]
 extern crate native_tls;
+extern crate percent_encoding;
 extern crate serde;
 extern crate serde_json;
 extern crate serde_urlencoded;
--- a/src/proxy.rs
+++ b/src/proxy.rs
@@ -5,10 +5,12 @@
 
 use http::{header::HeaderValue, Uri};
 use hyper::client::connect::Destination;
-use url::percent_encoding::percent_decode;
+use percent_encoding::percent_decode;
 use {IntoUrl, Url};
 use std::collections::HashMap;
 use std::env;
+#[cfg(feature = "socks")]
+use std::io;
 #[cfg(target_os = "windows")]
 use std::error::Error;
 #[cfg(target_os = "windows")]
@@ -330,11 +332,15 @@
         // Resolve URL to a host and port
         #[cfg(feature = "socks")]
         let to_addr = || {
-            let host_and_port = try_!(url.with_default_port(|url| match url.scheme() {
-                "socks5" | "socks5h" => Ok(1080),
-                _ => Err(())
-            }));
-            let mut addr = try_!(host_and_port.to_socket_addrs());
+            let host = try_!(url.host_str().ok_or_else(|| io::Error::new(io::ErrorKind::InvalidData, "URL has no host")));
+            let port = try_!(match url.port_or_known_default() {
+                Some(port) => Ok(port),
+                None => match url.scheme() {
+                    "socks5" | "socks5h" => Ok(1080),
+                    _ => Err(io::Error::new(io::ErrorKind::InvalidData, "URL has no port")),
+                }
+            });
+            let mut addr = try_!(format!("{}:{}", host, port).to_socket_addrs());
             addr
                 .next()
                 .ok_or_else(::error::unknown_proxy_scheme)
